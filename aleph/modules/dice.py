"""	A module that makes the ``roll`` command from the ``diceroll`` python library availible. """

import diceroll

from aleph.utils.modules import Module

dice = Module()

@dice.command
def roll (event, args):
	result = diceroll.roll(args if args else "1d6")
	event.reply("Rolled {}".format(result))
