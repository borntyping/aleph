""" The Handler interface and generic `Event` types that describe various reserved tags """

from abc import *

class Handler (object):
	"""
	An object that provides a file descriptor to the reactor, and proccess it's events.
	
	Handlers should also define a ``__protocol__`` attribute, which is used when the reactor spawns events for the handler (such as ``connect`` and ``disconnect``).
	"""
	
	__metaclass__ = ABCMeta
	
	@abstractmethod
	def fileno (self):
		"""	Return a file descriptor id for use in poll() """
		pass
	
	@abstractmethod
	def on_connect (self):
		"""	Initialise and start listening for input """
		pass
	
	def on_connect_complete (self):
		"""	on_connect() was successfull """
		pass
	
	def on_connect_fail (self, exception):
		"""	on_connect() failed """
		raise exception

	@abstractmethod
	def on_input (self):
		"""
		Handle select.POLLIN events, and return or yield an
		interable set of (tag, protocol, event) tuples.
		"""
		pass
	
	@abstractmethod
	def on_disconnect (self):
		"""	The EventHandler was disconnected """
		pass

class GenericMessage (object):
	""" An Event representing a message (tag:message) """
	
	def message_text (self):
		""" Returns the text of the message """
		raise NotImplementedError()
	
	def message_reply (self, text):
		""" Reply to the event """
		raise NotImplementedError()
