Design details
==============

Aleph consists primarily of two sections: the `Reactor` and the `Dispatcher`.

Reactor
--------

The `Reactor` manages `EventHandler` objects, which take input from anything that uses a file descriptor (primarily sockets) and outputs `Event` objects.

`EventHandler` objects are created from a protocol specific implementation (such as the `IRCClient`) and a configuration dictionary, containing directives such as the address to connect to.

Dispatcher
----------

The `Dispatcher` takes `Event` objects as input, and passes them into the modules it manages. Modules are collections of functions, which can be marked as listeners for specific protocol events, or for `MessageEvent` objects.
