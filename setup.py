#!/usr/bin/python

from setuptools import setup, find_packages
from aleph import __version__

setup(
    name             = 'Aleph',
    version          = __version__,

    author           = 'Sam Clements',
    author_email     = 'sam@borntyping.co.uk',
    url              = 'https://github.com/borntyping/aleph',
    
    description      = 'A python IRC library',
    long_description = open('README.rst').read(),
    
    packages         = find_packages(),
    
    entry_points = {
		'console_scripts': [
			'aleph = aleph:run',
			'aleph-run = aleph:run'
		],
        'aleph.protocols': [
            'irc = aleph.protocols.irc.main:IRCClient'
        ],
        'aleph.modules': [
            'irc = aleph.modules.irc:irc',
            'irc_commands = aleph.modules.irc:irc_commands',
            'irc_history = aleph.modules.irc:irc_history',
            'sed = aleph.modules.sed:sed',
            'dice = aleph.modules.dice:dice',
        ],
    },

)
