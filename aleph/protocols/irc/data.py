"""	Data objects - Channel, Hostmask """

import re

class Channel (object):
	"""	Channel class, providing an object for storing details on a channel """
	
	def __init__ (self, server, name):
		"""
		Create a channel object
		
		:Parameters:
			- `server` (Server): The server the channel belongs to
			- `name` (str): The name of the channel
		"""
		self.server = server
		self.name = name
		
		self.topic = ""
		self.flags = list()
		self.names = list()

	def __str__ (self):
		return self.name

class Hostmask (dict):
	"""	A dictionary describing a hostmask """
	
	_prefix = re.compile("^(?P<servername>[\w\-\.]+)|(?P<nick>[\w][\w\-\[\]\\`\^\{\}]+)(?:!(?P<user>[^@]+))?(?:@(?P<host>.+))?$")
	
	def __init__ (self, source):
		"""
		Generate the hostmask object using a regex on the source string
		
		:Parameters:
			- `source` (str): The hostmask to create the Hostmask object from
		"""
		
		# Populate the object with the results of the regex
		m = self._prefix.match(source)
		if m:
			self.update(m.groupdict())
		else:
			raise Exception("Incorrectly formatted hostmask")
	
	@property
	def name (self):
		"""	Returns a name from the hostmask """
		return self['servername'] if ('servername' in self) else self['nick']
	
	def __str__ (self):
		return self['servername'] if ('servername' in self) else "{nick}!{user}@{host}".format(**self)
