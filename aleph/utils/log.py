"""	Defaults for the logging module """

from logging import getLogger, Formatter

def getlog (object):
	return getLogger(object.__class__.__name__)

#: The default logging configuration
defaults = {
	'version':1,

	'loggers':{
		'': {
			'handlers':	['stream','file'],
			'level':	'INFO',
		},
	},

	'handlers':{
		'stream': {
			'class':		'logging.StreamHandler',
			'formatter':	'short',
		},

		'file':{
			'class':		'logging.FileHandler',
			'formatter':	'basic',
			'filename':		'aleph.log',
		},
	},

	'formatters':{
		'basic':{
			'format': "%(levelname)s:%(asctime)s:%(name)s:%(message)s"
		},

		'short':{
			'format': "%(levelname)-8s %(name)10s:%(message)s"
		},
	},
}

# Use colorlog if possible
try:
	import colorlog

	defaults['handlers']['stream']['formatter'] = 'colored'
	defaults['formatters']['colored'] = {
		'()': 'colorlog.ColoredFormatter',
		'format':"%(log_color)s%(levelname)-8s%(reset)s %(name)10s:%(message)s"
	}
except ImportError:
	pass
