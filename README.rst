Aleph
=====

Aleph is an easily-extensible library/chatbot. It's currently still under heavy development, but aims to provide a tool where new modules can easily be installed using both a normal package manger and/or `pip <http://pypi.python.org/pypi/pip/>`_. It does this though the use of pythons `pkg_resources <http://packages.python.org/distribute/pkg_resources.html>`_ module to load python objects associated with the ``aleph.protocol`` and ``aleph.module`` entry points.

Protocols
---------

Currently, Aleph only supports IRC, and intends to add XMPP and STDIN input in the near future.


Modules
-------

Aleph is sad and has no modules yet. :(
