Aleph documentation
===================

Aleph is a python IRC library.

Contents
========

.. toctree::
   :maxdepth: 2
   
   modules
   design
   todo
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

