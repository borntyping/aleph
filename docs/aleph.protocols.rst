protocols Package
=================

:mod:`generic` Module
---------------------

.. automodule:: aleph.protocols.generic
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    aleph.protocols.irc

