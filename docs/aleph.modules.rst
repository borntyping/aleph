modules Package
===============

:mod:`dice` Module
------------------

.. automodule:: aleph.modules.dice
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`irc` Module
-----------------

.. automodule:: aleph.modules.irc
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`sed` Module
-----------------

.. automodule:: aleph.modules.sed
    :members:
    :undoc-members:
    :show-inheritance:

