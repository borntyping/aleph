irc Package
===========

:mod:`data` Module
------------------

.. automodule:: aleph.protocols.irc.data
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`events` Module
--------------------

.. automodule:: aleph.protocols.irc.events
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`main` Module
------------------

.. automodule:: aleph.protocols.irc.main
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`parser` Module
--------------------

.. automodule:: aleph.protocols.irc.parser
    :members:
    :undoc-members:
    :show-inheritance:

