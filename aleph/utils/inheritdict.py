"""
InheritDict - a dictionary object that will check a parent object for an item if it cannot find it
"""

class InheritDict (dict):	
	def __init__ (self, iterable, parent):
		dict.__init__(self, iterable)
		self.parent = parent
	
	def __contains__ (self, key):
		"""	Key in self or key in parent """
		return dict.__contains__(self, key) or key in self.parent
	
	def __getitem__ (self, key):
		"""	Get the item from the parent, if self does not have the key and parent has """
		if dict.__contains__(self, key):
			return dict.__getitem__(self, key)
		elif self.parent.__contains__(key):
			return self.parent.__getitem__(key)
		else:
			raise KeyError("'{}'".format(key))
		
	def get (self, key, default = None):
		"""	Get the value for key from the InheritDict, or return a given default value """
		if self.__contains__(key):
			return self.__getitem__(key)
		else:
			return default

	def __str__ (self):
		"""	Return a sting representation of the ``InheritDict`` and it's parent """
		return "%s -> %s" % (dict.__str__(self), str(self.parent))
